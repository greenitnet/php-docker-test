FROM php:7.4-apache
maintainer "Tomasz Szymański tomasz.szymanski@greenit.com.pl" 

LABEL maintainer="tomasz.szymanski@greenit.com.pl"
LABEL io.k8s.description "Test benchmark applicaiton" 
LABEL io.openshift.non-scalable     false
LABEL io.openshift.min-memory  512Mi
LABEL io.openshift.min-cpu     0.5
LABEL io.openshift.tags php
USER root
RUN apt-get update && apt-get install -y \
    libcap2-bin \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
RUN sed -i "s/80/8080/g" /etc/apache2/sites-available/000-default.conf /etc/apache2/ports.conf && \
    sed -ri -e 's!combined!json!g' /etc/apache2/sites-enabled/000-default.conf /etc/apache2/sites-available/000-default.conf  
COPY www/ /var/www/html/
COPY apache/apache2.conf /etc/apache2/apache2.conf
expose 8080/tcp
user www-data
